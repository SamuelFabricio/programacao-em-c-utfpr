/*6) Ler três valores inteiros que representam os lados de um triângulo e 
determinar se esses valores podem formar um triângulo 
(obs.: para ser um triângulo cada lado deve ser menor que a soma dos outros 
dois lados). Se for um triângulo, determinar o seu tipo:
equilátero (todos os lados iguais), 
isósceles (dois lados iguais) e 
escaleno (todos os lados diferentes)*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero1, numero2, numero3, triangulo;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero1);
    printf("|Digite outro numero|");
    scanf("%i",&numero2);
    printf("|Digite outro numero|");
    scanf("%i",&numero3);
    //Processamento
    if (numero1<numero2+numero3 && numero2<numero1+numero3 && numero3<numero1+numero2)
    {
        if (numero1==numero2 && numero2==numero3)
        {
            printf("Triangulo Equilatero|");
        }
        else if (numero1==numero2 && numero3!=numero1 || numero1==numero3 && numero2!=numero1|| numero2==numero3 && numero1!=numero2)
        {
            printf("|Triangulo Isosceles|");
        }
        else
        {
            printf("|Triangulo Escaleno|");
        }
        
    }
    else
    {
        printf("|Nao pode ser triangulo|");
    }
    
    
    //Saída de Dados;
    
    return 0;
}