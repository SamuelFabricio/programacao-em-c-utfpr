/*5) Calcular o imposto de renda de uma pessoa de acordo com a seguinte tabela:
Renda anual Alíquota* Até R$ 10.000,00 0%
> R$ 10.000,00 e <= R$ 25.000,00 10%
Acima de R$ 25.000,00 25%
*Alíquota é o percentual para realizar o cálculo do imposto de renda a ser pago. 
Se informado valor negativo, não realizar o cálculo e informar o usuário.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salBruto, salLiquido;
    //Entrada de Dados
    printf("|Digite o valor do salario|");
    scanf("%f",&salBruto);
    //Processamento
    if (salBruto<=10000)
    {
        salLiquido=salBruto;
    }
    else if (salBruto>10000 && salBruto<=25000)
    {
        salLiquido=salBruto*0.9;
    }
    else if (salBruto>25000)
    {
        salLiquido=salBruto*0.75;
    }
    else
    {
        printf("|ERRO|");
    }
    
    //Saída de Dados;
    printf("|Salario Liquido| R$%.2f |",salLiquido);
    return 0;
}