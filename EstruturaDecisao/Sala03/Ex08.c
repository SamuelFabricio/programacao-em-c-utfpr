/*8) Fazer o programa para o algoritmo representado no fluxograma a seg*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salBase, salBruto, salLiquido, gratificacao, impRenda;
    //Entrada de Dados
    printf("|Digite o Salario Base|");
    scanf("%f",&salBase);
    printf("|Digite a Gratificacao|");
    scanf("%f",&gratificacao);
    //Processamento
    salBruto = salBase + gratificacao;
    //Saída de Dados;
    if (salBruto>1000)
    {
        impRenda=0.85;
        salLiquido=salBruto*impRenda;
    }
    else if (salBruto<=1000)
    {
        impRenda=0.80;
        salLiquido=salBruto*impRenda;
    }
    
    printf("|Salario Liquido| R$%.2f|\n",salLiquido);
    return 0;
}