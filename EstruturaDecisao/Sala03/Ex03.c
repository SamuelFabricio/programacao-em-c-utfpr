/*3) Ler três valores inteiros diferentes e colocá-los em ordem crescente. 
Os valores devem ser apresentados com uma instrução:
printf("Menor: %d Meio: %d Maior: %d\n", menor, meio, maior);
Sugestão: Dividir o problema em partes: encontrar o maior, o menor e o do meio
separadamente. Armazenar os valores em variável e mostrá-los com uma instrução.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero1, numero2, numero3, meioNumero, menorNumero, maiorNumero;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero1);
    printf("|Digite outro numero|");
    scanf("%i",&numero2);
    printf("|Digite outro numero|");
    scanf("%i",&numero3);
    //Processamento
    if (numero1>numero2 && numero2>numero3)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero1,numero2,numero3); 
    }
    else if (numero1>numero3 && numero3>numero2)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero1,numero3,numero2);
    }
    else if (numero3>numero1 && numero1>numero2)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero3,numero1,numero2);
    }else if (numero3>numero2 && numero2>numero1)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero3,numero2,numero1);
    }
    else if (numero2>numero1 && numero1>numero3)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero2,numero1,numero3);
    }
    else if (numero2>numero3 && numero3>numero1)
    {
        printf("|%i|Maior |%i|Meio |%i|Menor|",numero2,numero3,numero1);
    }
    else
    {
        printf("|ERRO|");
    }
    
    
}