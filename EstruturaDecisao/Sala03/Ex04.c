/*4) Ler o gênero (F ou f para feminino, M ou m para masculino. Para qualquer outro
caractere informar que é inválido e finalizar o programa). Se informado um caractere
válido, ler a altura da pessoa e calcular e mostrar o seu peso ideal, utilizando as seguintes
fórmulas:
a) Para homens: (72.7 * h) - 58;
b) Para mulheres: (62.1 * h) - 44.7.*/

#include <stdio.h>
#include <ctype.h>

int main()
{
    //Declarar Variáveis
    char sex,sexo;
    float altura, pesoIdealM,pesoIdealF;
    //Entrada de Dados
    printf("|Digite| 'f' feminino | 'm' masculino |");
    scanf("%c",&sex);
    printf("|Digite sua altura|");
    scanf("%f",&altura);
    //Processamento
    sexo=toupper(sex);
    switch (sexo)
    {
    case 'F':
            pesoIdealF=(62.1 * altura) - 44.7;
            printf("|O seu peso ideal seria %.1fKg|\n",pesoIdealF);
        break;
    case 'M':
            pesoIdealM=(72.7 * altura) - 58;
            printf("|O seu peso ideal seria %.1fKg|\n",pesoIdealM);
        break;
    default:
        printf("|ERRO|");
        break;
    }
    return 0;
}