/*9) Números palíndromos são aqueles que escritos da direita para a esquerda 
ou da esquerda para a direita tem o mesmo valor. Ex.: 9229, 4554, 9779. 
Fazer um programa que dado um número de 4 dígitos, calcular e escrever 
se este número é ou não palíndromo.*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero,digito1, digito2, digito3, digito4;
    //Entrada de Dados
    printf("|Digite o 1 numero de 4 digitos|");
    scanf("%i",&numero);
    //Processamento
    digito1 = (int)(numero/1000);
    digito2 = (int)(numero%1000/100);
    digito3 = (int)(numero%100/10);
    digito4 = (int)(numero%10);
    //Processamento;
    printf("%i",digito1);
    printf("%i",digito2);
    printf("%i",digito3);
    printf("%i ",digito4);
    //Saída de Dados;
    if (digito1==digito4 && digito2==digito3)
    {
        printf("|Este numero eh um palindromo|");
    }
    else
    {
        printf("|Este numero nao eh um Palindromo");
    }
    
    return 0;
}