/*10) Faça um programa que solicite ao usuário o valor do salário de um 
funcionário e apresente o menu a seguir e permita ao usuário escolher a 
opção desejada e mostre o resultado. Verifique a possibilidade de opção 
inválida e não se preocupe com restrições, como salário negativo. 
Use switch - case, if e if - else para a solução. Menu de opções:
1 – Imposto
2 – Novo salário
3 – Classificação
Digite a opção desejada:
Na opção 1 - Calcular e mostrar o valor do imposto sobre o salário 
usando as regras a seguir:
Salário Percentual do imposto
Menor que R$ 1.000,00 5%
De R$ 1.000,00 a R$ 1.500,00 10%
Acima de R$ 1.500,00 15%
Na opção 2 - Mostrar o valor de aumento de salário e o valor final do 
salário (salário a umentado) usando a tabela a seguir:
Salário Aumento
Menor que R$ 1.000,00 R$ 75,00
De R$ 1.000,00 a R$ 1.500,00 R$ 100,00
Acima de R$ 1.500,00 R$ 150,00
Na opção 3 - Mostrar a classificação usando a tabela a seguir:
Salário Classificação
Maior ou igual a R$ 1.000,00 Categoria A
Menor que R$ 1.000,00 Categoria B*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salBruto;
    int menu;
    //Entrada de Dados
    printf("|Digite o salario|R$:");
    scanf("%f",&salBruto);
    printf("|MENU DE OPCOES|\n|1| Imposto\n|2| Novo Salario\n|3| Classificacao\n");
    scanf("%i",&menu);
    //Processamento
    switch (menu)
    {
    case 1:
        if (salBruto<1000)
        {
            printf("|Salario Liquido R$%.2f|",salBruto*0.95);
        }
        else if (salBruto>=1000 && salBruto<1500)
        {
            printf("|Salario Liquido R$%.2f|",salBruto*0.90);
        }
        else if (salBruto>=1500)
        {
            printf("|Salario Liquido R$%.2f|",salBruto*0.85);
        }
        break;
    case 2:
        if (salBruto<1000)
        {
            printf("|Salario com aumento| R$%.2f|",(salBruto+75));
        }
        else if (salBruto>=1000 && salBruto<1500)
        {
            printf("|Salario com aumento| R$%.2f|",(salBruto+100));
        }
        else if (salBruto>=1500)
        {
            printf("|Salario com aumento| R$%.2f|",(salBruto+150));
        }
        break;
    case 3:
        if (salBruto>=1000)
        {
            printf("|Categoria A|");
        }
        else if (salBruto<1000)
        {
            printf("|Categoria B|");
        }
        
        break;
    default:
        break;
    }
    //Saída de Dados;
    
    return 0;
}