/*7) Ler um número e utilizando uma estrutura if else if else if... 
(obrigatoriamente encadeada) informar se ele é:
a) Divisível por 5, por 3 ou por 2;
Exemplo: 30 é divisível por 2, 3 e 5. b) Se ele é divisível somente por 5 e 
por 3; por 5 e por 2; ou por 3 e por 2;
Exemplo: 15 é divisível somente por 3 e por 5. Exemplo: 10 é divisível 
somente por 5 e por 2. Exemplo: 6 é divisível somente por 3 e por 2. c) 
Divisível somente por 5, por 3 ou por 2;
Exemplo: 25 divisível somente por 5
d) Não é divisível por nenhum destes;
Exemplo: 7 não é divisível por 5, 3 ou 2;*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero);
    //Processamento
    if (numero%5==0 && numero%3==0 && numero%2==0)
    {
        printf("|%i eh divisivel por 5, 3 e 2|",numero);
    }
    else if (numero%5==0 && numero%3==0 && numero%2!=0)
    {
        printf("|%i eh divisivel por 5 e 3|",numero);
    }
    else if (numero%5==0 && numero%3!=0 && numero%2==0)
    {
        printf("|%i eh divisivel por 5 e 2|",numero);
    }
    else if (numero%5!=0 && numero%3==0 && numero%2==0)
    {
        printf("|%i eh divisivel por 3 e 2|",numero);
    }
    else if (numero%5==0 && numero%3!=0 && numero%2!=0)
    {
        printf("|%i eh divisivel apenas por 5|",numero);
    }
    else if (numero%5!=0 && numero%3==0 && numero%2!=0)
    {
        printf("|%i eh divisivel apenas por 3|",numero);
    }
    else if (numero%5!=0 && numero%3!=0 && numero%2==0)
    {
        printf("|%i eh divisivel apenas por 2|",numero);
    }
    //Saída de Dados;
    
    return 0;
}