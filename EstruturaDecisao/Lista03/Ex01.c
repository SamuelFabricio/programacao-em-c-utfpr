/*1) Elabore um programa que obtenha por meio do teclado a pontuação, em valores
inteiros, de um candidato ao concurso vestibular, nas seguintes área: exatas (e), 
humanas(h) e conhecimento geral (cg). A seguir deverá ser definida a média 
ponderada usando a seguinte expressão: mp = ((e * 3) + (h * 2) + cg) / 6. 
A média ponderada calculada define o rendimento do candidato de acordo com 
a seguinte tabela:
Insuficiente = 0 - 250 pontos
Baixo = 251 - 500 pontos
Regular = 501 - 700 pontos
Bom = 701 - 900 pontos
Excelente = maior que 900 pontos*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int humanas,exatas,conGeral;
    float medPonderada;
    //Entrada de Dados
    printf("|Digite| Nota de Humanas |");
    scanf("%i",&humanas);
    printf("|Digite| Nota de Exatas |");
    scanf("%i",&exatas);
    printf("|Digite| Nota de Conhecimentos Gerais |");
    scanf("%i",&conGeral);
    //Processamento
    medPonderada = ((exatas * 3) + (humanas * 2) + conGeral) / 6;
    //Saída de Dados;
    if (medPonderada<=250)
    {
        printf("|Insuficiente|");
    }
    else if (medPonderada>=251 && medPonderada<=500)
    {
        printf("|Baixo|");
    }
    else if (medPonderada>=501 && medPonderada<=700)
    {
        printf("|Regular|");
    }
    else if (medPonderada>=701 && medPonderada<=900)
    {
        printf("|Bom|");
    }
    else if (medPonderada>=901 && medPonderada<=1000)
    {
        printf("|Excelente|");
    }
    else
    {
        printf("|ERRO|");
    }
    printf("|%i Pontos|",(int)medPonderada);
    
    
    
    
    
    return 0;
}