/*A função toupper() transforma um caractere para maiúsculo. Para usar esta 
função é necessário incluir no cabeçalho a biblioteca ctype.h. Veja o exemplo 
a seguir:
#include <stdio.h>
#include <ctype.h>
int main(void)
{
char ch;
printf("Digite um caracter: ");
scanf("%c", &ch);
ch = toupper(ch));
printf("Maiusculo: %c", ch);
return 0;
}
Faça um programa que receba um caractere via teclado, transforme esse caractere 
em maiúsculo e verifique se ele pertence ou não ao alfabeto (A, B, ..., Z). 
Caso ele pertença, o programa deve gerar a saída printf("O caracter digitado 
pertence ao albafeto\n")e, com o uso da estrutura switch - case, verificar se 
o caractere é uma vogal (neste caso, a saída deve ser printf("VOGAL %c\n", ch)) 
ou uma consoante (neste caso, a saída deve ser printf("CONSOANTE %c\n", ch)). 
Caso o caractere não pertença ao alfabeto, o programa deverá gerar a saída 
printf("O caracter digitado nao pertence ao alfabeto\n").*/

#include <stdio.h>
#include<ctype.h>

int main()
{
    //Declarar Variáveis
    char letra, letraMaius, pertAlfabeto;
    //Entrada de Dados
    printf("|Digite uma letra|");
    scanf("%c",&letra);
    //Processamento
    letraMaius = toupper(letra);
    pertAlfabeto = isalpha(letra);
    //Saída de Dados;
    if (pertAlfabeto)
    {
        printf("|Pertence ao Alfabeto|\n");
        switch (letra)
        {
            case 'a':
                printf("|%c| VOGAL |\n",letraMaius);
                break;
            case 'e':
                printf("|%c| VOGAL |\n",letraMaius);
                break;
            case 'i':
                printf("|%c| VOGAL |\n",letraMaius);
                break;
            case 'o':
                printf("|%c| VOGAL |\n",letraMaius);
                break;
            case 'u':
                printf("|%c| VOGAL |\n",letraMaius);
                break;
            default:
                printf("|%c| CONSOANTE |\n",letraMaius);
                break;
        }
    }
    else
    {
        printf("|Caractere nao pertence ao Alfabeto|");
    }
    
    
    return 0;
}