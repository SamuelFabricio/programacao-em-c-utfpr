/*2) Escreva um programa que leia um número e 
verifique se ele é maior, menor ou igual a 10.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero);
    
    //Processamento
    
    //Saída de Dados;
    if (numero<10)
    {
        printf("|%i menor que 10|",numero);
    }
    else if (numero==10)
    {
        printf("|%i igual a 10|",numero);
    }
    else if (numero>10)
    {
        printf("|%i maior que 10|",numero);
    }
    else
    {
        printf("|ERRO|");
    }
    
    return 0;
}