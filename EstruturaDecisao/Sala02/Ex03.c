/*3) Elaborar um programa que leia uma letra. Se informado ‘F’ ou ‘f’ 
mostrar a mensagem “pessoa física”, se informado ‘J’ ou ‘j’ mostrar a mensagem 
“pessoa jurídica” ou "caractere inválido" para qualquer outro caractere.
Exemplo:
char pessoa; //declarar uma variável do tipo char
scanf(“%c”, &pessoa); //para ler um char
if(pessoa == ‘f’ || pessoa == ‘F’) //para comparar se o conteúdo
armazenado na variável é um determinado caractere.*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    char pessoa;
    //Entrada de Dados
    printf("|Digite 'f' ou 'F' pessoa fisica|\n|Digite 'j' ou 'J' pessoa juridica|\n");
    scanf("%c",&pessoa);
    
    //Processamento
    
    //Saída de Dados;
    if (pessoa =='f' || pessoa =='F')
    {
        printf("|Pessoa Fisica|");
    }
    else if (pessoa =='j' || pessoa =='J')
    {
        printf("|Pessoa Juridica|");
    }
    else
    {
        printf("|ERRO|");
    }
    
    return 0;
}