/*8) Faça um programa que leia três notas de um aluno e calcule a média ponderada, com os pesos 1, 3 e 4,
respectivamente, e:
a) Se a média obtida está entre 6 a 10 informar que o aluno está aprovado;
b) Se a média obtida está entre 4 e 5,9 informar que o aluno está em recuperação. Nesse caso, ler a nota de
recuperação e calcular a média final (que é a média entre a média anual e a nota de recuperação);
b.1) Se a média final é menor que 5 informar que o aluno está reprovado após recuperação;
b.2) Se é igual ou maior que 5 informar que o aluno está aprovado após recuperação;
c) Se a média obtida é menor que 4 informar que o aluno está reprovado antes da recuperação*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float nota1, nota2, nota3, notaRec, media, mediaFinal;
    //Entrada de Dados
    printf("|Digite a primeira nota|");
    scanf("%f",&nota1);
    printf("|Digite a segunda nota|");
    scanf("%f",&nota2);
    printf("|Digite a terceira nota|");
    scanf("%f",&nota3);
    //Processamento
    media = ((nota1)+(nota2*3)+(nota3*4))/8;
    mediaFinal = media+notaRec/2;
    //Saída de Dados;
    if (media>=6 && media<=10)
    {
        printf("|Aprovado com media %.1f|\n",media);
    }
    else if (media>=4 && media<6)
    {
        printf("|Recuperacao com media %.1f|\n",media);
        printf("|Digite a nota da recuperacao|\n");
        scanf("%f",&notaRec);
        if (mediaFinal>=5 && mediaFinal<=10)
        {
            printf("|Aprovado apos recuperacao com media %.1f|\n",mediaFinal);
        }
        else if (mediaFinal>=0 && mediaFinal<5)
        {
            printf("|Reprovado apos recuperacao com media %.1f|\n",mediaFinal);
        }
        else
        {
            printf("|ERRO|");
        }
        
    }
    else if (media>=0 && media<4)
    {
        printf("|Reprovado sem recuperacao com media %.1f|\n",media);
    }
    else
    {
        printf("|ERRO|");
    }
    printf("%f",mediaFinal);
    return 0;
}