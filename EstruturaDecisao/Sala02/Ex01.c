/*1) Elaborar um programa que leia um número que representa uma senha e 
verifica se a mesma está correta ou não, comparando-a com 12345 e 
informar "Acesso autorizado" ou "Acesso negado", conforme o caso.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int senha;
    //Entrada de Dados
    printf("|Digite a senha|");
    scanf("%i",&senha);
    
    //Processamento
    
    //Saída de Dados;
    if (senha==12345)
    {
        printf("|Acesso Autorizado|");
    }
    else
    {
        printf("|Acesso Negado|");
    }
    
    return 0;
}