/*5) Escreva um programa que leia um número e verifique se ele se encontra 
fora do intervalo entre 5 e 20.
Mostre uma mensagem se o número está fora desse intervalo.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero);
    
    //Processamento
    if (numero>=5 && numero<=20)
    {
        printf("|O numero %i nao pertence ao intervalo de 5 a 20|\n", numero);
    }
    else
    {
        printf("|ERRO|\n", numero);
    }
    
    //Saída de Dados;
    printf("|Programa Finalizado|");
    return 0;
}