/*6) Elaborar um programa que leia um valor que se refere à altura de uma 
pessoa e mostre uma mensagem conforme a tabela a seguir. 
Utilizar variável do tipo float.
Altura Informação mostrada
Menor que 1,50 "Altura abaixo de um metro e cinquenta centímetros"
De 1,50 a 1,80 "Altura entre um metro e cinquenta e um metro e oitenta centímetros"
Maior que 1,80 "Altura acima de um metro e oitenta centímetros*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float altura;
    //Entrada de Dados
    printf("|Digite sua altura|");
    scanf("%f",&altura);
    //Processamento
    if (altura<=1.5)
    {
        printf("|Altura abaixo de um metro e cinquenta centimetros|");
    }
    else if (altura>1.5 && altura<=1.8)
    {
        printf("|Altura entre um metro e cinquenta e um metro e oitenta centimetros|");
    }
    else if (altura>1.8)
    {
        printf("|Altura acima de um metro e oitenta centimetros|");
    }
    else
    {
        printf("Informacao incorreta|");
    }
    
    return 0;
}