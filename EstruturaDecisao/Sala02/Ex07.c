/*7) Elaborar um programa que lê dois valores, verifica se o primeiro é 
múltiplo do segundo e escreve a mensagem "São múltiplos" ou "Não são múltiplos" 
dependendo da condição. Verificar para que não seja realizada uma divisão 
por zero. Nesse caso, informar que não é possível realizar uma divisão por zero.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero1, numero2;
    //Entrada de Dados
    printf("|Digite um numero|\n");
    scanf("%i",&numero1);
    printf("|Digite outro numero !=0|\n");
    scanf("%i",&numero2);
    //Processamento
    if (numero2!=0)
    {
        if (numero1%numero2==0)
        {
            printf("|Sao multiplos|");
        }
        else
        {
            printf("Nao sao multiplos|");
        }
        
    }
    else
    {
        printf("|Nao eh possivel realizar divisao por zero|");
    }
    
    
    return 0;
}