/*1) A contribuição para o INSS é calculada da seguinte forma:
a) Salário bruto até três salários mínimos = INSS 8%. b) Salário bruto acima 
de três salários mínimos = INSS 10%. c) Para as contribuições maiores que o 
salário mínimo, considerar a importância de um salário mínimo. 
Elaborar um programa que receba como entrada o valor do salário mínimo e o 
valor do salário bruto, calcule o INSS e o salário líquido restante e 
informe-os*/

#include <stdio.h>
#define INSS8 0.08
#define INSS10 0.1
int main()
{
    //Declarar Variáveis
    float salBruto, salMinimo, salLiquido, salCalculo;
    //Entrada de Dados
    printf("|Digite| Salario Bruto |\n");
    scanf("%f",&salBruto);
    printf("|Digite| Valor do Salario Minimo|");
    scanf("%f",&salMinimo);
    //Processamento
    
    if (salBruto/salMinimo<=1)
    {
        printf("|VALOR DO SALARIO LIQUIDO|R$%.2f|\n|DESCONTO INSS|R$%.2f|\n",salBruto,0.00);
    }
    else if (salBruto/salMinimo>1 && salBruto/salMinimo<=3)
    {
        printf("|VALOR DO SALARIO LIQUIDO|R$%.2f|\n|DESCONTO INSS|R$%.2f|\n",salBruto*(1-INSS8),salBruto*INSS8);
    }
    else if (salBruto/salMinimo>3)
    {
        printf("|VALOR DO SALARIO LIQUIDO|R$%.2f|\n|DESCONTO INSS|R$%.2f|\n",salBruto*(1-INSS10),salBruto*INSS10);
    }
    else
    {
        printf("|VALOR INFORMADO ESTA INCORRETO|");
    }
    //Saída de Dados;
    printf("|Programa Finalizado|");
    return 0;
}