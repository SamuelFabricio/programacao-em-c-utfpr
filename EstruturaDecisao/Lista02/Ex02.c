/*2) Faça um programa que apresente o menu a seguir e permita ao usuário 
escolher a opção desejada, receba os dados necessários para executar a 
operação e mostre o resultado.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int opcao, a, b, c;
    //Entrada de Dados
    printf("|DIGITE UM NUMERO INTEIRO|");
    scanf("%i",&a);
    printf("|DIGITE OUTRO NUMERO INTEIRO|");
    scanf("%i",&b);
    printf("|DIGITE OUTRO NUMERO INTEIRO|");
    scanf("%i",&c);
    printf("|MENU DE OPCOES|\n|1|MOSTRAR NUMEROS EM ORDEM CRESCENTE\n|2|MOSTRAR NUMEROS EM ORDEM DECRESCENTE\n|3|MOSTRAR NUMEROS QUE SAO MULTIPLOS DE 2\n");
    printf("|DIGITE A OPCAO DESEJADA:");
    scanf("%i",&opcao);
    //Processamento
    switch (opcao)
    {
    case 1:
            if (a>b && b>c)
            {
                printf("|%i > %i > %i|\n",a,b,c);
            }
            else if (a>c && c>b)
            {
                printf("|%i > %i > %i|\n",a,c,b);
            }
            else if (b>a && a>c)
            {
                printf("|%i > %i > %i|\n",b,a,c);
            }
            else if (b>c && c>a)
            {
                printf("|%i > %i > %i|\n",b,c,a);
            }
            else if (c>a && a>c)
            {
                printf("|%i > %i > %i|\n",c,a,b);
            }
            else if (c>b && b>a)
            {
                printf("|%i > %i > %i|\n",c,b,a);
            }
        break;
    case 2:
            if (a>b && b>c)
            {
                printf("|%i > %i > %i|\n",c,b,a);
            }
            else if (a>c && c>b)
            {
                printf("|%i > %i > %i|\n",b,c,a);
            }
            else if (b>a && a>c)
            {
                printf("|%i > %i > %i|\n",c,a,b);
            }
            else if (b>c && c>a)
            {
                printf("|%i > %i > %i|\n",a,c,b);
            }
            else if (c>a && a>c)
            {
                printf("|%i > %i > %i|\n",b,a,c);
            }
            else if (c>b && b>a)
            {
                printf("|%i > %i > %i|\n",a,b,c);
            }
        break;
    case 3:
        if (a%2==0 && b%2==0 && c%2==0)
        {
            printf("|Os numeros %i, %i, %i, sao multiplos de 2|",a,b,c);
        }
        else if (a%2==0 && b%2==0)
        {
            printf("|Os numeros %i, %i, sao multiplos de 2|",a,b);
        }
        else if (a%2==0)
        {
            printf("|O numero %i eh multiplo de 2|",a);
        }
        else if (a%2==0 && c%2==0)
        {
            printf("|Os numeros %i, %i, sao multiplos de 2|",a,c);
        }
        else if (b%2==0 && c%2==0)
        {
            printf("|Os numeros %i, %i, sao multiplos de 2|",b,c);
        }
        else if (b%2==0)
        {
            printf("|O numero %i eh multiplo de 2|",b);
        }
        else if (c%2==0)
        {
            printf("|O numero %i eh multiplo de 2|",c);
        }
        else
        {
            printf("|Nenhum numero eh multiplo de 2|");
        }
        
        break;
    default:
        printf("|ERRO|");
        break;
    }
    return 0;
}