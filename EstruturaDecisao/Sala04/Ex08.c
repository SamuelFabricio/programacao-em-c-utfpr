/*8) Implemente um programa que adivinhe o "número mágico" entre 0 e 10. O programa deverá
imprimir a mensagem "Certo! %d é o número mágico" quando o jogador acerta o número mágico, a mensagem "Errado, muito alto", caso o jogador tenha digitado um número maior que o número
mágico e a mensagem "Errado, muito baixo", caso o jogador tenha digitado um número menor
que o número mágico. O número mágico é produzido usando o gerador de números randômicos
de C (função rand(), que exige o uso da biblioteca stdlib.h).*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    //Declarar Variáveis
    int numero, randNumero;
    //Entrada de Dados
    printf("|Digite um numero entre 0 e 10|");
    scanf("%i",&numero);
    //Processamento
    srand(time(NULL));
    randNumero = rand() % (10+1);
    //Saída de Dados;
    if (numero==randNumero)
    {
        printf("Acertou, voce venceu!");
    }
    else if (numero>randNumero)
    {
        printf("Perdeu, muito alto!");
    }
    else if (numero<randNumero)
    {
        printf("Perdeu, muito baixo!");
    }
    else
    {
        printf("Erro!");
    }
    
    return 0;
}