/*2) Ler dois números inteiros e informar:
a) Se ambos são divisíveis por 5.
b) Se pelo menos um deles é divisível por 5. 
c) Se ambos são pares. 
d) Se pelo menos um deles é ímpar.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero1, numero2, impar;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero1);
    printf("|Digite um numero|");
    scanf("%i",&numero2);
    //Processamento
    if (numero1%5==0 && numero2%5==0)
    {
        printf("|Ambos sao divisiveis por 5|\n");
    }
    else if (numero1%5==0 || numero2%5==0)
    {
        if (numero1%5==0)
        {
            printf("|O numero %i eh divisivel por 5|\n",numero1);
        }
        else if (numero2%5==0)
        {
            printf("|O numero %i eh divisivel por 5|\n",numero2);
        }
    }
    else if (numero1%2==0 && numero2%2==0)
    {
        printf("|Ambos sao pares|\n");
    }
    if (numero1%2!=0 || numero2%2!=0)
    {
        if (numero1%2!=0)
        {
            printf("|Numero %i eh impar|\n",numero1);
        }
        else if (numero2%2!=0)
        {
            printf("|Numero %i eh impar|\n",numero2);
        }
    } 
    
    //Saída de Dados;
    
    return 0;
}