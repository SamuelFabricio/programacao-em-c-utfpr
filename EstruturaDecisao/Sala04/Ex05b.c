/*5.2) Considera-se o problema de esperar para jantar em um restaurante. 
O objetivo é o programa
aprender a definição para DeveEsperar? Existem os seguintes atributos para 
descrever as
situações:
Fregueses: Quantas pessoas estão no restaurante (nenhuma, algumas, cheio)?
Tempo de espera (em minutos): 0-10, 10-30, 30-60, >60. Alternativa: Há um 
restaurante alternativo na redondeza?
Reserva: Foi feita uma reserva?
Bar: Existe um bar confortável onde possa se esperar?
Sex/Sab: É sexta ou sábado?
Faminto: Está com fome?
Chovendo: Está chovendo lá fora?
As entradas são do tipo char, exceto o tempo de espera que pode ser int. 
Portanto, estabeleça
uma letra para cada entrada esperada, por exemplo 'S' para sim e 'N' para não.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    char quantPessoas, faminto,chuva, restProximo, reserva, bar, fimSemana;
    int tempoEspera;
    //Entrada de Dados
    printf("Devo Esperar para ser atendido neste restaurante?\n");
    printf("|Quantas pessoas tem no restaurante?|\n|n|nenhuma\n|a|alguns\n|c|cheio\n");
    scanf("%c",&quantPessoas);
    fflush(stdin);
    switch (quantPessoas)
    {
    case 'n':
        printf("Nao\n");
        break;
    case 'a':
        printf("Sim\n");
        break;
    case 'c':
        printf("Quanto tempo de espera? [minutos]");
        scanf("%i",&tempoEspera);
        fflush(stdin);
        if (tempoEspera<=10)
        {
            printf("Sim\n");
        }
        else if (tempoEspera>10 && tempoEspera<=30)
        {
            printf("Esta faminto? [s/n]");
            scanf("%c",&faminto);
            fflush(stdin);
            switch (faminto)
            {
            case 'n':
                printf("Sim\n");
                break;
            case 's':
                printf("Esta chovendo? [s/n]");
                scanf("%c",&chuva);
                fflush(stdin);
                switch (chuva)
                {
                case 'n':
                    printf("Nao\n");
                    break;
                case 's':
                    printf("Sim\n");
                    break;
                
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
        else if (tempoEspera>30 && tempoEspera<=60)
        {
            printf("Tem algum restaurante proximo? [s/n]");
            scanf("%c",&restProximo);
            fflush(stdin);
            switch (restProximo)
            {
            case 'n':
                printf("Foi feito reserva? [s/n]");
                scanf("%c",&reserva);
                switch (reserva)
                {
                case 'n':
                    printf("Existe um bar confortavel para esperar? [s/n]");
                    scanf("%c",&bar);
                    switch (bar)
                    {
                    case 'n':
                        printf("Nao\n");
                        break;
                    case 's':
                        printf("Sim\n");
                        break;
                    default:
                        break;
                    }
                    break;
                case 's':
                    printf("Sim\n");
                    break;
                default:
                    break;
                }
                break;
            case 's':
                printf("Eh sexta ou sabado?");
                scanf("%c",&fimSemana);
                fflush(stdin);
                switch (fimSemana)
                {
                case 's':
                    printf("Sim\n");
                    break;
                case 'n':
                    printf("Nao\n");
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
        else if (tempoEspera>60)
        {
            printf("Nao\n");
        }
        else
        {
            printf("ERRO\n");
        }
        
        break;
    default:
        printf("ERRO\n");
        break;
    }
    printf("%c",quantPessoas);
    return 0;
}