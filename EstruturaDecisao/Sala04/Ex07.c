/*) Crie um programa que leia uma data no formato ddmmaaaa e imprima se a 
data é válida ou
não*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int dia, mes, ano, data;
    //Entrada de Dados
    printf("|Digite a data| [ddmmaaaa]");
    scanf("%i",&data);
    dia = data/1000000;
    mes = data%1000000/10000;
    ano = data%10000;
    //Processamento
    if (ano%4==0 && ano%100!=0)
    {
        if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
        {
            if (dia>0 && dia<32)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else if (mes==4 || mes==6 || mes==9 || mes==11)
        {
            if (dia>0 && dia<31)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
        }
        else if (mes==2)
        {
            if (dia>0 && dia<30)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else
        {
            printf("Data incorreta");
        }
        
    }
    else
    {
        if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
        {
            if (dia>0 && dia<32)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else if (mes==4 || mes==6 || mes==9 || mes==11)
        {
            if (dia>0 && dia<31)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
        }
        else if (mes==2)
        {
            if (dia>0 && dia<29)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else
        {
            printf("Data incorreta");
        }
        
    }
    
    
    return 0;
}