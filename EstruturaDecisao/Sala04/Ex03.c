/*3) Elabore um programa que leia o dia e o mês de nascimento de uma pessoa 
e determine o seu signo conforme a tabela a seguir:
Intervalo Signo
de 22/12 até 20/1 Capricórnio
de 21/1 até 19/2 Aquário
de 20/2 até 20/3 Peixes
de 21/3 até 20/4 Áries
de 21/4 até 20/5 Touro
de 21/5 até 20/6 Gêmeos
de 21/6 até 21/7 Câncer
de 22/7 até 22/8 Leão
de 23/8 até 22/9 Virgem
de 23/9 até 22/10 Libra
de 23/10 até 21/11 Escorpião
de 22/11 até 21/12 Sagitário
Se informada uma data que não corresponde aos intervalos indicados, informar que a data é
inválida.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int dia, mes;
    //Entrada de Dados
    printf("|Digite o dia de nascimento|");
    scanf("%i",&dia);
    printf("|Digite o mes de nascimento|");
    scanf("%i",&mes);
    //Processamento
    if (dia<0 || dia>31 || mes>0 || mes>12)
    {
        printf("|A data inserida esta incorreto|");
    }
    else if (dia>=22 && mes==12 || dia<=20 && mes==1)
    {
        printf("|Capricornio|");
    }
    else if (dia>=21 && mes==1 || dia<=19 && mes==2)
    {
        printf("|Aquario|");
    }
    else if (dia>=20 && mes==2 || dia<=20 && mes==3)
    {
        printf("|Peixes|");
    }
    else if (dia>=21 && mes==3 || dia<=20 && mes==4)
    {
        printf("|Aries|");
    }
    if (dia>=21 && mes==4 || dia<=20 && mes==5)
    {
        printf("|Touros|");
    }
    if (dia>=21 && mes==5 || dia<=20 && mes==6)
    {
        printf("|Gemeos|");
    }
    if (dia>=21 && mes==6 || dia<=21 && mes==7)
    {
        printf("|Cancer|");
    }
    if (dia>=22 && mes==7 || dia<=22 && mes==8)
    {
        printf("|Leao|");
    }
    if (dia>=23 && mes==8 || dia<=22 && mes==9)
    {
        printf("|Virgem|");
    }
    if (dia>=23 && mes==9 || dia<=22 && mes==10)
    {
        printf("|Libra|");
    }
    if (dia>=23 && mes==10 || dia<=21 && mes==11)
    {
        printf("|Escorpiao|");
    }
    if (dia>=22 && mes==12 || dia<=21 && mes==1)
    {
        printf("|Sargitario|");
    }
    //Saída de Dados;
    
    return 0;
}