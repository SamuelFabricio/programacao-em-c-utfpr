/*4) Um ano é bissexto se for divisível por 4 e não for divisível por 100. 
Também são bissextos os divisíveis por 400. Escreva um programa que determina 
se um ano informado pelo usuário é bissexto.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int ano;
    //Entrada de Dados
    printf("|Digite o ano|");
    scanf("%i",&ano);
    //Processamento
    if (ano%4==0 && ano%100!=0)
    {
        printf("|%i eh um ano Bissexto|",ano);
    }
    else
    {
        printf("|%i nao eh um ano Bissexto|",ano);
    }
    
    //Saída de Dados;
    return 0;
}