/*6) Crie um programa que leia dia, mês e ano separadamente e 
imprima se a data é válida ou não.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int dia, mes, ano;
    //Entrada de Dados
    printf("|Digite o dia|");
    scanf("%i",&dia);
    printf("|Digite o mes|");
    scanf("%i",&mes);
    printf("|Digite o ano|");
    scanf("%i",&ano);
    //Processamento
    if (ano%4==0 && ano%100!=0)
    {
        if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
        {
            if (dia>0 && dia<32)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else if (mes==4 || mes==6 || mes==9 || mes==11)
        {
            if (dia>0 && dia<31)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
        }
        else if (mes==2)
        {
            if (dia>0 && dia<30)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else
        {
            printf("Data incorreta");
        }
        
    }
    else
    {
        if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
        {
            if (dia>0 && dia<32)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else if (mes==4 || mes==6 || mes==9 || mes==11)
        {
            if (dia>0 && dia<31)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
        }
        else if (mes==2)
        {
            if (dia>0 && dia<29)
            {
                printf("Data correta");
            }
            else
            {
                printf("Data incorreta");
            }
            
        }
        else
        {
            printf("Data incorreta");
        }
        
    }
    
    
    return 0;
}