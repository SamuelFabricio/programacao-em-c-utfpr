/*1) O código a seguir tem o objetivo de obter o resto da divisão do número 
maior pelo menor informados (independentemente da ordem informada) e 
informar que não é possível realizar a divisão caso o divisor seja zero. 
Faça os ajustes necessários no código para que esses objetivos
sejam alcançados.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero1, numero2, resultado;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero1);
    printf("|Digite um numero|");
    scanf("%i",&numero2);
    //Processamento
    if (numero1>numero2)
    {
        resultado=numero1%numero2;
    }
    else if (numero2>numero1)
    {
        resultado=numero2%numero1;
    }
    
    //Saída de Dados;
    printf("|O resto da divisao eh %i|",resultado);
    return 0;
}