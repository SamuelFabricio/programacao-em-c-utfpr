/*5) Uma árvore de decisão obtém a decisão pela execução de uma sequência de 
testes. Cada nó interno da árvore corresponde a um teste do valor de uma 
das propriedades e os ramos deste nó são identificados com os possíveis 
valores do teste. Cada nó folha da árvore especifica o valor de retorno se a 
folha for atingida.*/

#include <stdio.h>
#include <ctype.h>

int main()
{
    //Declarar Variáveis
    char saldo, aplicacoes;
    //Entrada de Dados
    printf("|Saldo em conta?|[Sim/Nao]");
    scanf("%c",&saldo);
    switch (toupper(saldo))
    {
    case 'S':
        printf("|Cliente sem risco|");
        break;
    case 'N':
        printf("|Possui Aplicacoes?[Sim/Nao]|");
        fflush(stdin);
        scanf("%c",&aplicacoes);
        switch (toupper(aplicacoes))
        {
        case 'S':
            printf("|Cliente sem risco|");
            break;
        case 'N':
            printf("|Cliente com risco|");
            break;
        default:
            printf("|Caractere errado|");
            break;
        }
        break;
    default:
        printf("|Caractere errado|");
        break;
    }
    
    return 0;
}