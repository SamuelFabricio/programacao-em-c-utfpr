/*5) Elaborar um programa que, dada a idade de um nadador, 
classifique-o em uma das seguintes categorias:
Infantil A = 5 a 7 anos
Infantil B = 8 a 10 anos
Juvenil A = 11 a 13 anos
Juvenil B = 14 a 17 anos
Sênior = maiores de 17 anos
Não categorizado como atleta = menor de 5 anos
Mas se o valor informado é negativo, informar ao usuário que o valor 
é inválido para o escopo da solução.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int ano, idade;
    //Entrada de Dados
    printf("|Digite o ano seu ano de nascimento|");
    scanf("%i",&ano);
    //Processamento
    idade = 2021-ano;
    //Saída de Dados;
    if (idade<=0 && idade<=100)
    {
        printf("|DATA DE NASCIMENTO INVALIDA|");
    }
    
    else if (idade>0 && idade<5)
    {
        printf("|NAO ESTA NO ESCOPO DE ATLETA|");
    }
    else if (idade>=5 && idade<=7)
    {
        printf("|CATEGORIA INFANTIL A|");
    }
    else if (idade>=8 && idade<=10)
    {
        printf("|CATEGORIA INFANTIL B|");
    }
    else if (idade>=11 && idade<=14)
    {
        printf("|CATEGORIA JUVENIL A|");
    }
    else if (idade>=14 && idade<=17)
    {
        printf("|CATEGORIA JUVENIL B|");
    }
    else if (idade>17 && idade<=100)
    {
        printf("|CATEGORIA SENIOR|");
    }
    return 0;
}