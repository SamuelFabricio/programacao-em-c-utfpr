/*4) Ler um número double:
a) Se o número lido é menor que 100, separar a parte inteira e a parte decimal 
e mostrálas separadamente. 
b) Se o número lido é maior ou igual a 100, obter o resto da divisão desse 
número por 3 e mostrar o resto. */

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    double numero;
    //Entrada de Dados
    printf("|Digite um numero Double|");
    scanf("%lf",&numero);
    if (numero<100)
    {
        printf("|Parte Inteira %i|\n|Parte Decimal %lf|",(int)numero,numero-(int)numero);
    }
    else if (numero-(int)numero>0.5)
    {
        printf("|Resto da divisao por 3 eh %i|",((int)numero+1)%3);
    }
    else
    {
        printf("|Resto da divisao por 3 eh %i|",((int)numero)%3);
    }
    
    return 0;
}