/*1) Ler um número inteiro e calcular e apresentar o seu módulo ou valor 
absoluto (númerosem sinal), isto é, se o número é negativo apresentá-lo 
como positivo.*/

#include <stdio.h>

int main(void)
{
    //Declarar Variáveis
    int numero;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%i",&numero);

    if (numero >= 0)
    {
        printf("|Numero Positivo|");
    }
    else
    {
        printf("|Numero Negativo|");
    }
    return 0;
}