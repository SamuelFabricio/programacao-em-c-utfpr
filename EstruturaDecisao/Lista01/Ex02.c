/*2) Ler dois valores numéricos e apresentar a diferença entre eles. 
A diferença sempre é positiva, portanto, o menor é subtraído do maior não 
importando a ordem em que os números são informados.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float numero1, numero2;
    //Entrada de Dados
    printf("|Digite um numero|");
    scanf("%f",&numero1);
    printf("|Digite um numero|");
    scanf("%f",&numero2);
    if (numero1>numero2)
    {
        printf("|%.0f - %.0f = %.0f|\n",numero1,numero2,numero1-numero2);
    }
    else
    {
        printf("|%.0f - %.0f = %.0f|\n",numero2,numero1,numero2-numero1);
    }
    printf("|Programa Finalizado|");
    return 0;
}