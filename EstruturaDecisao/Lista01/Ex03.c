/*3) Faça um programa que leia o gênero, e caso o mesmo seja 'f', 'F', 'm' 
ou 'M' leia também a idade e o tempo de trabalho de uma pessoa e determine 
se ela pode ou não se aposentar. Assuma que homens se aposentam com 45 anos 
de trabalho ou idade superior a 70 anos e mulheres se aposentam com 40 anos 
de trabalho ou idade superior a 65 anos.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    char sexo;
    int idade,trabalho;
    //Entrada de Dados
    printf("|Digite (F) para genero feminino e (M) para genero masculino|");
    scanf("%c",&sexo);
    printf("|Digite sua idade|");
    scanf("%i",&idade);
    printf("|Digite o total de anos trabalhados|");
    scanf("%i",&trabalho);
    if (sexo=='F'||'f')
    {
        if (trabalho>=40)
        {
            printf("|Voce pode aposentar|");
        }
        else if (idade>=70)
        {
            printf("|Voce pode aposentar|");
        }
        else
        {
            printf("|Voce nao pode aposentar ainda|");
        }
        
        
    }
    else if (sexo=='M'||'m')
    {
        if (trabalho>=45)
        {
            printf("|Voce pode aposentar|");
        }
        else if (idade>=75)
        {
            printf("|Voce pode aposentar|");
        }
        else
        {
            printf("|Voce nao pode aposentar ainda|");
        }
    }
    else
    {
        printf("|Erro reinicie|");
    }  
    return 0;
}