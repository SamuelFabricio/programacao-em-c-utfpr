/*FOR
2) Apresentar os múltiplos de 5, entre 5 e 50 separados por tabulação.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int i;
    //Entrada de Dados
    //Processamento
    for ( i = 5; i <= 50; i++)
    {
        if (i%5==0)
        {
            printf("%i\t",i);
        }
        
    }
    
    //Saída de Dados;
    return 0;
}