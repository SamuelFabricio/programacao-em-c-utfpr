/*8) Fazer um programa que faça o levantamento dos candidatos que se 
inscreveram para vagas em uma empresa. Considerando que para cada candidato, 
a empresa tenha obtido as seguintes informações: 
- Idade
- Nacionalidade (B - Brasileiro ou E - Estrangeiro)
- Possui curso superior (S - Sim ou N - Não)
Faça um programa para determinar:
a) A quantidade de brasileiros. 
b) A quantidade de estrangeiros. 
c) A idade média dos brasileiros sem curso superior. 
d) A menor idade entre os estrangeiros com curso superior. 
Estabelecer uma condição para finalizar a entrada de dados do programa. 
Por exemplo,igual a 0 ou idade negativa. Se a idade for igual a 0 ou negativa, 
não ler as informações de nacionalidade e se possui curso superior,
e sair do programa.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int menorIdadeEtCS=9999999, contIdadeBrsCS=0, somaIdadeBrsCS=0, contEstrangeiro=0, contBrasileiro=0, idade=1;
    float media;
    char nacionalidade, cursoSuperior;
    //Entrada de Dados
    while (idade>0)
    {
        printf("|Informe a idade|\t");
        scanf("%i",&idade);
        if (idade!=0)
        {
        printf("|Informe a nacionalidade|\n[B]Brasileiro [E]Estrangeiro:\t");
        fflush(stdin);
        scanf("%c",&nacionalidade);
        if (nacionalidade=='B' || nacionalidade=='b')
        {
            contBrasileiro++;
        }
        else if (nacionalidade=='E' || nacionalidade=='e')
        {
            contEstrangeiro++;
        }
        fflush(stdin);
        printf("|Informe se possui curso superior|\n[S]Sim [N]Nao:\t");
        scanf("%c",&cursoSuperior);
        if (cursoSuperior=='N' || cursoSuperior=='n')
        {
            if (nacionalidade=='B' || nacionalidade=='b')
            {
                contIdadeBrsCS++;
                somaIdadeBrsCS=somaIdadeBrsCS+idade;
            }
            
        }
        else if (cursoSuperior=='S' || cursoSuperior=='s')
        {
            if (nacionalidade=='E' || nacionalidade=='e')
            {
                if (idade<menorIdadeEtCS)
                {
                    menorIdadeEtCS=idade;
                }
                
            }
            
        }
        
        }
        
    }
    //Processamento
    media = somaIdadeBrsCS/contIdadeBrsCS;
    //Saída de Dados;
    printf("|%i brasileiros|\n",contBrasileiro);
    printf("|%i estrangeiros|\n",contEstrangeiro);
    printf("|%.1f media de idade dos brasileiros sem curso superior|\n",media);
    printf("|%i anos o estrageiro com curso superior de menor idade|",menorIdadeEtCS);
    return 0;
}