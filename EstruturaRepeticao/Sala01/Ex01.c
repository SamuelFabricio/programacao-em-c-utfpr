/*FOR
1) Apresentar todos os números entre 20 e 35 separados por tabulação*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int i;
    //Entrada de Dados
    //Processamento
    for ( i = 20; i <= 35; i++)
    {
        printf("%i\t",i);
    }
    
    //Saída de Dados;
    return 0;
}