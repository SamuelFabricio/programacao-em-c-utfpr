/*DO - WHILE
9) Ler um número que indica a quantidade de ímpares iniciando em 1 que deve ser
mostrada. O valor informado para a quantidade deve ser maior que 0. Validar 
a entrada.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int impares=0, i, numeroMax;
    //Entrada de Dados
    do
    {
        printf("|Digite o numero final do intervalo|");
        scanf("%i",&numeroMax);
    } while (numeroMax<=0);
    //Processamento
    for ( i = 1; i <= numeroMax; i++)
    {
        if (i%2!=0)
        {
            impares++;
        }
        
    }
    
    //Saída de Dados;
    printf("|%i numero(s) impar(es) no intervalo|",impares);
    return 0;
}