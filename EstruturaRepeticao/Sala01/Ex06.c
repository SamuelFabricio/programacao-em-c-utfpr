/*FOR
6) Ler dois números que representam os limites de um intervalo e ler o incremento.
Mostrar os números desse intervalo de acordo com o incremento indicado e em ordem
crescente. O usuário pode informar os valores que representam os limites do 
intervalo em ordem crescente ou decrescente. Fazer a média dos ímpares e 
divisíveis por 35 do intervalo*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int soma=0, cont=0, i, numero1, numero2, valorIncremento;
    float media;
    //Entrada de Dados
    printf("|Digite valor inicial do intervalo|");
    scanf("%i",&numero1);
    printf("|Digite valor final do intervalor|");
    scanf("%i",&numero2);
    printf("|Digite o valor do incremento|");
    scanf("%i",&valorIncremento);
    //Processamento
    if (numero1>numero2)
    {
        for ( i = numero2; i <= numero1; i=i+valorIncremento)
        {
            printf("%i\t",i);
            if (i%2!=0 && i%35==0)
            {
                cont++;
                soma = soma = i;
            }
            
        }
        
    }
    else
    {
        for ( i = numero1; i <= numero2; i=i+valorIncremento)
        {
            printf("%i\t",i);
            if (i%2!=0 && i%35==0)
            {
                cont++;
                soma = soma = i;
            }
        }
        
    }
    media = soma/cont;
    //Saída de Dados;
    printf("\n|MEDIA de Impares multiplos de 35| %.1f\n",media);
    return 0;
}