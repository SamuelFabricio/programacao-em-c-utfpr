/*WHILE
7) Ler uma série de números indicados pelo usuário até ser informado o valor zero.
Encontrar e mostrar o maior e o menor dos valores informados pelo usuário. 
O valor zero indica o final da leitura e não deve ser considerado.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero=1,maior=-99999999,menor=99999999,troca=0;
    //Entrada de Dados
    while (numero>0 || numero<0)
    {
        printf("|Digite um numero|");
        scanf("%i",&numero);
        if (numero>maior && numero!=0)
        {
            maior = numero;
        }
        else if (numero<menor && numero!=0)
        {
            menor = numero;
        }
        
    }
    
    //Processamento
    
    //Saída de Dados;
    printf("Maior numero %i\n",maior);
    printf("Menor numero %i\n",menor);
    return 0;
}