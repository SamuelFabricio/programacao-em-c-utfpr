/*DO - WHILE e FOR
5) Ler um número maior que 2 e imprimir todos os pares entre 2 e o número lido. 
Imprimir a soma dos pares, o produto dos ímpares que são divisíveis 
por 9 e a média de todos os números do intervalo.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int cont=0, produto=1, soma=0,numero, i;
    float media;
    //Entrada de Dados
    printf("|Digite um numero maior que 2|");
    scanf("%i",&numero);
    //Processamento
    for ( i = 2; i <= numero; i++)
    {
        if (i%2==0)
        {
            cont++;
            printf("%i\t",i);
            soma=soma+i;
        }
        else
        {
            if (i%9==0)
            {
                produto=produto*i;
            }
            
        }
        
    }
    media=soma/cont;
    //Saída de Dados;
    printf("\n|SOMA| %i",soma);
    printf("\n|PRODUTO| %i",produto);
    printf("\n|MEDIA| %.1f",media);
    return 0;
}