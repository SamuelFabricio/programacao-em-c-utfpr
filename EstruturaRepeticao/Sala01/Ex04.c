/*FOR
4) Apresentar os números entre 10 e 0, ou seja, em ordem decrescente, 
separados por tabulação.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int i;
    //Entrada de Dados
    //Processamento
    for ( i = 10; i >= 0; i--)
    {
        printf("%i\t",i);
    }
    
    //Saída de Dados;
    return 0;
}