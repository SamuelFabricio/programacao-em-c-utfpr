/*10) Uma indústria fabrica roupas categorizadas em masculinas, 
femininas e infantis. Ler a quantidade e a respectiva categoria (M, F ou I). 
Após o loop, calcular e mostrar o percentual de produtos por categoria. 
Validar para que seja informada uma categoria válida. Finalizar a leitura 
quando informado um valor 0 ou negativo para a quantidade. Ler inicialmente 
a quantidade e depois a categoria. Se informada uma quantidade negativa, 
não ler a categoria.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int cont=0, somaM=0, somaF=0, somaI=0, quantidade;
    char categoria;
    float porcentagem;
    //Entrada de Dados
    do
    {
        printf("|Informe a quantidade|");
        scanf("%i",&quantidade);
        fflush(stdin);
        if (quantidade>0)
        {   
            cont++;
            do
            {
                printf("|Informe a categoria|");
                scanf("%c",&categoria);
                if (categoria=='M' || categoria=='m')
                {
                    somaM=somaM+quantidade;
                }
                else if (categoria=='F' || categoria=='f')
                {
                    somaF=somaF+quantidade;
                }
                else if (categoria=='I' || categoria=='i')
                {
                    somaI=somaI+quantidade;
                }
                fflush(stdin);
            } while (categoria!='M' && categoria!='m' && categoria!='F' && categoria!='f' && categoria!='I' && categoria!='i'); 
        }
    } while (quantidade>0);
    //Processamento
    porcentagem = (somaM+somaF+somaI)/100;
    //Saída de Dados;
    printf("%.1f%% Masculino\n",somaM/(porcentagem));
    printf("%.1f%% Feminino\n",somaF/(porcentagem));
    printf("%.1f%% Infantil\n",somaI/(porcentagem));
    return 0;
}