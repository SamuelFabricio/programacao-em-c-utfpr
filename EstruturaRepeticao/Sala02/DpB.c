#include <stdio.h>
#define TAM 41 //Constante
int main()
{
    //Declarar Variáveis
    int numero,binario[TAM],i; //binario é um vetor; a constante é nº de posições.
    //Entrada de Dados
    printf("|Digite um numero decimal|");
    scanf("%i", &numero);
    //Processamento
    for ( i = TAM -1; i >= 0; i--)
    {
        binario[i]=(numero%2)==0 ? 0 : 1; //Operador ternário ? [V]0:1[F]
        numero = numero/2; //Cada volta o numero é dividido por 2;
    }
    for ( i = 1; i < TAM; i++)
    {
        printf("%i",binario[i]); 
        if((i%4)==0) // casa para separar os espaços.
            printf(" ");
    }
    return 0;
}