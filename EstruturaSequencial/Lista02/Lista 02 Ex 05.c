#include <stdio.h>

int main()
{
    float vdiaria;
    float vdiariadesc;
    float nquartos;
    printf("Informe o valor da diaria:\n");
    scanf("%f",&vdiaria);
    printf("Informe a quantidade de apartamentos:\n");
    scanf("%f",&nquartos);
    vdiariadesc = vdiaria*0.75;
    printf("O valor da diaria promocional: R$%.2f.\n",vdiariadesc);
    printf("O valor total arrecadado com 100%% de ocupa�ao: R$%.2f.\n",(nquartos*vdiariadesc)*2);
    printf("O valor total arrecadado com 70%% de ocupa�ao: R$%.2f.\n",(((nquartos*0.7)*vdiariadesc)*2));
    printf("O valor que deixara de arrecadar com o desconto: R$%.2f.\n",((nquartos*vdiaria)-(nquartos*vdiariadesc))*2);
    printf("Programa Finalizado.\n");
    return 0;
}
