#include <stdio.h>

int main()
{
    int segundo;
    float minuto;
    float hora;
    float dia;
    printf("Digite o tempo em segundos:\n");
    scanf("%i",&segundo);
    minuto =  segundo/60;
    hora = minuto/60;
    dia = hora/24;
    printf("%.1d segundo(s).\n",segundo);
    printf("%.1f minuto(s).\n",minuto);
    printf("%.1f hora(s).\n",hora);
    printf("%.1f dia(s).\n",dia);
    printf("Programa Finalizado!\n");
    return 0;
}
