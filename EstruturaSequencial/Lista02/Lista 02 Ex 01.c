#include <stdio.h>
#include <math.h>

int main()
{
    float custo;
    float valoringresso;
    float cobrircusto;
    float lucro;
    printf("Informe o custo do espetaculo:\n");
    scanf("%f",&custo);
    printf("Informe o valor do ingresso:\n");
    scanf("%f",&valoringresso);
    cobrircusto = ceil(custo/valoringresso);
    lucro = ceil(cobrircusto*1.25);
    printf("Para cobrir o custo do espetaculo eh necessario vender %.0f ingressos.\n",cobrircusto);
    printf("Para obter 25%% de lucro eh necessario vender %.0f ingressos.\n ",lucro);
    return 0;
}
