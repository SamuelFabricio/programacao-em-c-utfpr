/*2) Considerando que para um consórcio sabe-se o número total de 
prestações, a quantidade de prestações pagas e o valor de cada 
prestação (que é fixo). Escreva um programa que determine o valor 
total já pago pelo consorciado e o saldo devedor.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float prestacaoTot, prestacaoPaga, valorPrestacao;
    //Entrada de Dados
    printf("|Digite| Total de Prestacoes |");
    scanf("%f",&prestacaoTot);
    printf("|Digite| Prestacoes Pagas |");
    scanf("%f",&prestacaoPaga);
    printf("|Digite| Valor Fixo da Parcela |");
    scanf("%f",&valorPrestacao);
    //Processamento
    
    //Saída de Dados;
    printf("|Total Pago| R$:%.2f\n",prestacaoPaga*valorPrestacao);
    printf("|Saldo Devedor| R$:%.2f\n",(prestacaoTot-prestacaoPaga)*valorPrestacao);
    return 0;
}