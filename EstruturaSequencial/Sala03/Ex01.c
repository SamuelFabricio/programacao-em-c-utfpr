/*1) Ler um número double. Separar a parte inteira e a parte decimal 
desse número. Apresentar a parte decimal como um valor double e 
como um inteiro de três dígitos. Da parte inteira separar o número 
que representa unidade, dezena e centena e mostrar.
*/

#include <stdio.h>
#include <math.h>
int main()
{
    //Declarar Variáveis
    double numero;
    //Entrada de Dados
    printf("|Digite um numero Double|");
    scanf("%lf",&numero);
    //Processamento
    
    //Saída de Dados;
    printf("|Numero Informado| %f\n",numero);
    printf("|Parte Inteira| %i\n",(int)numero);
    printf("|Parte Decimal| %f\n",numero-(int)numero);
    printf("|Parte Decimal com 3 Primeiros Digitos| %i\n",(int)(1000*(numero-(int)numero)));
    printf("|Centena(s)| %.0f\n",floor(numero/100));
    printf("|Dezena(s)| %i\n",((int)numero%100)/10);
    printf("|Unidade(s)| %i\n",(int)numero%100%10);
    return 0;
}