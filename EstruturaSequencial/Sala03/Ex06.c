/*6) Ler um número inteiro longo (long int) que representa os segundos 
e convertê-lo para horas, minutos e segundos. Mostrar a quantidade 
de horas, minutos e segundos obtidos, no seguinte formato:
xhoras:yminutos:zsegundos.
Exemplo: Informado o valor 3725, apresentar:
1hora:2minutos:5segundos
*/
#include <stdio.h>
#include <math.h>
int main()
{
    //Declarar Variáveis
    long int numero;
    float horas,minutos,segundos;
    //Entrada de Dados
    printf("|Digite um numero inteiro|");
    scanf("%li",&numero);
    //Processamento
    horas = numero/3600;
    minutos = (numero-(3600*horas))/60;
    segundos = (minutos-(int)minutos)*60;
    //Saída de Dados;
    printf("|%.0f|HORA(s) |%.0f|MINUTO(s) |%.0f|SEGUNDO(s)",horas,floor(minutos),segundos);
    return 0;
}