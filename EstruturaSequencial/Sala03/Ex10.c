/*10) (DESAFIO) Obter o resto da divisão de dois valores inteiros 
informados pelo usuário, sem usar o operador de resto.*/
#include <stdio.h>
#include <math.h>
int main()
{
    //Declarar Variáveis
    float numero1, numero2, divisao, decimal, processamento;
    //Entrada de Dados
    printf("|Digite| 1' Numero |");
    scanf("%f",&numero1);
    printf("|Digite| 2' Numero |");
    scanf("%f",&numero2);
    //Processamento
    divisao = numero1/numero2;
    decimal = divisao-(int)divisao;
    processamento = decimal*numero2;
    //Saída de Dados;
    printf("|%.0f/%.0f = %i|\n",numero1,numero2,(int)divisao);
    printf("|Resto = %.0f|\n",processamento);
    return 0;
}