/*5) Faça um programa para calcular a quantidade de fita necessária, 
em centímetros, para amarrar um pacote com duas voltas, sendo uma 
pela largura e outra pelo comprimento do pacote. Serão fornecidas a 
largura, a altura e o comprimento do pacote. Para amarrar as pontas 
da fita são necessários 15 cm de fita em cada ponta. A figura a seguir 
ilustra a maneira como a fita é passada pelo pacote.*/

/*Prof pelo que eu entendi terá que sobrar 15cm em cada ponta do laço
isso resulta em 60cm extra, na lista o ex teste está dando 290 e o meu
está dando 320.*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float fita1, fita2, largura, altura, comprimento;
    //Entrada de Dados
    printf("|Insira| Comprimento |");
    scanf("%f",&comprimento);
    printf("|Insira| Altura |");
    scanf("%f",&altura);
    printf("|Insira| Largura |");
    scanf("%f",&largura);
    //Processamento
    fita1 = 30+(2*comprimento)+(2*altura);
    fita2 = 30+(2*largura)+(2*altura);
    //Saída de Dados;
    printf("| Sera necessario %f cm de fita |",fita1+fita2);
    return 0;
}