/*7) Suponha que um caixa disponha apenas de cédulas de R$ 100, 
R$ 10 e R$ 1. Escreva um programa para ler o valor de uma compra e o 
valor fornecido pelo usuário para pagar essa compra, e calcule o troco.
Calcular e mostrar a quantidade de cada tipo de cédula que o caixa 
deve fornecer como troco. Mostrar também o valor da compra e do troco. 
Use variáveis do tipo int*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int valorCompra, valorPago, nota100, nota10, nota1, troco;
    //Entrada de Dados
    printf("|Digite| Valor da Compra |");
    scanf("%i",&valorCompra);
    printf("|Digite| Valor Pago |");
    scanf("%i",&valorPago);
    //Processamento
    troco = valorPago-valorCompra;
    nota100 = troco/100;
    nota10 = (troco%100)/10;
    nota1 = troco%100%10;
    //Saída de Dados;
    printf("|Valor da compra: R$%i,00|\n|Valor Pago: R$%i,00|\n|Troco: R$%i,00\n",valorCompra,valorPago,troco);
    printf("|%i| NOTA(s) de R$100,00\n",nota100);
    printf("|%i| NOTA(s) de R$10,00\n",nota10);
    printf("|%i| NOTA(s) de R$1,00\n",nota1);
    return 0;
}