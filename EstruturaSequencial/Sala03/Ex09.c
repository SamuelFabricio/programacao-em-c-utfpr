/*9) Criar um programa que leia o peso (em kg) de uma pessoa e 
calcule e imprima:
a) O peso da pessoa em gramas.
b) O novo peso, em gramas, se a pessoa engordar 12%*/

#include <stdio.h>
#define PORCENTAGEM 1.12
int main()
{
    //Declarar Variáveis
    float peso;
    //Entrada de Dados
    printf("|Digite seu peso(kg)|");
    scanf("%f",&peso);
    
    //Processamento

    //Saída de Dados;
    printf("|Seu peso em gramas: %.0f gramas.\n",peso*1000);
    printf("|Engordando 12%% pesara: %.2f gramas.\n",(peso*PORCENTAGEM)*1000);
    printf("|Engordando 12%% pesara: %.2f(kg).\n",peso*PORCENTAGEM);
    return 0;
}