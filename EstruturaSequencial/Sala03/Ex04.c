/*4) Fazer um programa que leia um número inteiro de até três dígitos 
(considere que será fornecido um número de até 3 dígitos), 
calcule e imprima a soma dos seus dígitos.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    double numero, fatnumero, fatCentena, fatDezena, fatUnidade;
    //Entrada de Dados
    printf("|Digite| Numero inteiro de 3 digitos |");
    scanf("%lf",&numero);
    //Processamento
    fatCentena = (int)numero/100;
    fatDezena = ((int)numero%100)/10;
    fatUnidade = (int)numero%100%10;
    //Saída de Dados;
    printf("|%.0f| %.0f+%.0f+%.0f=%.0f",numero,fatCentena,fatDezena,fatUnidade,fatCentena+fatDezena+fatUnidade);
    return 0;
}

