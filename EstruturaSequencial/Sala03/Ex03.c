/*3) Leia um valor double que representa o troco a ser fornecido 
por um caixa. Separe a parte inteira (reais) da parte decimal 
(centavos) e apresente na forma: 123 reais e 18 centavos.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    double troco;
    //Entrada de Dados
    printf("|Digite| Valor do troco | R$");
    scanf("%lf",&troco);
    //Processamento
    
    //Saída de Dados;
    printf("|Valor Informado| %i reais e %.0f centavos.",(int)troco,(troco-(int)troco)*100);
    return 0;
}