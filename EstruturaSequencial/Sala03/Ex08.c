/*8) Ler um número que representa a quantidade de dias. 
Informe os anos (considere-os com 360 dias), meses
(considere-os com 30 dias) e os dias contidos nesse valor.*/

#include <stdio.h>
#include <math.h>
int main()
{
    //Declarar Variáveis
    long int numero;
    float anos,meses,dias;
    //Entrada de Dados
    printf("|Digite o numero de dias|");
    scanf("%li",&numero);
    //Processamento
    anos = numero/360;
    meses = (numero-(360*(int)anos))/30;
    dias = (numero-(360*(int)anos))%30;
    //Saída de Dados;
    printf("|%.0f|ANO(s) |%.0f|MES(es) |%.0f|DIA(s)",anos,floor(meses),dias);
    return 0;
}