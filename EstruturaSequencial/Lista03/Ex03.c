/*3) Ler um número inteiro com até 4 dígitos. Separar os dígitos desse número 
e mostrá-los em linhas distintas*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero; 
    float num1, num2, num3, num4;
    //Entrada de Dados
    printf("Digite um numero inteiro com 4 digitos|");
    scanf("%i",&numero);
    //Processamento
    num1 = numero/1000;
    num2 = (numero%1000)/100;
    num3 = (numero%1000/10%10);
    num4 = (numero%1000%10);
    //Saída de Dados;
    printf("|1'Digito|%.0f\n",num1);
    printf("|2'Digito|%.0f\n",num2);
    printf("|3'Digito|%.0f\n",num3);
    printf("|4'Digito|%.0f\n",num4);
    return 0;
}