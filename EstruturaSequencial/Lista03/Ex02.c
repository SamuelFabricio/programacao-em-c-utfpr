/*2) Fazer um programa para ler o salário de uma pessoa, o percentual de 
aumento e o percentual de descontos. Os descontos incidem sobre o salário 
com aumento. Calcular o novo salário e mostrá-lo como no exemplo a seguir*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salario, taxaAum, taxaDesc, salAumento, salLiquido;
    //Entrada de Dados
    printf("|Digite| Valor do Salario |\nR$");
    scanf("%f",&salario);
    printf("|Digite| Percentual de Aumento [10 para 10%%] |\n|");
    scanf("%f",&taxaAum);
    printf("|Digite| Percentual de Desconto [10 para 10%%] |\n|");
    scanf("%f",&taxaDesc);
    //Processamento
    salAumento = salario*(1+taxaAum*0.01);
    salLiquido = salAumento*(1-taxaDesc*0.01);
    //Saída de Dados;
    printf("|Salario Atual| R$:%.2f|\n",salario);
    printf("|Taxa de Aumento| %.0f%%|\n",taxaAum);
    printf("|Taxa de Desconto| %.0f%%|\n",taxaDesc);
    printf("|Salario com Aumento| R$:%.2f|\n",salAumento);
    printf("|Salario com Desconto| R$:%.2f|\n",salLiquido);
    printf("|PROGRAMA FINALIZADO| |PROGRAMA FINALIZADO|");
    return 0;
}