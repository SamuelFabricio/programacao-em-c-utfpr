/*5) Ler um número inteiro com até 3 dígitos. Separar os dígitos desse número 
e mostrá-los em linhas distintas.
Na sequência calcular e mostrar o inverso do número*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float numero, num1, num2, num3;
    //Entrada de Dados
    printf("|Digite 3 numeros inteiros|");
    scanf("%f",&numero);
    //Processamento
    num1 = (int)numero/100;
    num2 = (int)numero%100/10;
    num3 = (int)numero%10;
    //Saída de Dados;
    printf("|O inverso eh: %.0f%.0f%.0f|",num3,num2,num1);
    return 0;
}