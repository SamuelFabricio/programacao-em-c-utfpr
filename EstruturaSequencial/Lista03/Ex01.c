/*1) Fazer um programa que leia um valor double que representa o salário 
de uma pessoa. Apresente separadamente os reais (parte inteira) e os centavos 
(parte decimal).Observação: Apresentar os centavos como inteiro de dois dígitos 
(exemplo: 40 em vez de 0.40)*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    double salario, parteInt, parteDec;
    //Entrada de Dados
    printf("|Digite| Valor do Salario |");
    scanf("%lf",&salario);
    //Processamento
    parteInt = (int)salario;
    parteDec = (salario - parteInt)*100;
    //Saída de Dados;
    printf("|Salario R$%.2f|\n",salario);
    printf("|Parte em Reais: %.0f|\n",parteInt);
    printf("|Parte em Centavos: %.0f|\n",parteDec);
    return 0;
}