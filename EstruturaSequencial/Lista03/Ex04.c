/*4) Ler um número inteiro com até 5 dígitos. Separar os dígitos desse número 
e mostrá-los em linhas distintas.
Também calcular e mostrar a soma dos dígitos.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float numero, num1, num2, num3, num4, num5;
    //Entrada de Dados
    printf("|Digite um numero inteiro de 5 digitos|");
    scanf("%f",&numero);
    //Processamento
    num1 = (int)numero/10000;
    num2 = (int)numero%10000/1000;
    num3 = (int)numero%1000/100;
    num4 = (int)numero%100/10;
    num5 = (int)numero%10;
    //Saída de Dados;
    printf("|1|DIGITO eh %.0f\n",num1);
    printf("|2|DIGITO eh %.0f\n",num2);
    printf("|3|DIGITO eh %.0f\n",num3);
    printf("|4|DIGITO eh %.0f\n",num4);
    printf("|5|DIGITO eh %.0f\n",num5);
    printf("|A dos digitos eh: %.0f|\n",num1+num2+num3+num4+num5);
    printf("|PROGRAMA FINALIZADO| |PROGRAMA FINALIZADO|");
    return 0;
}