/*11) O que faz o algoritmo a seguir representado em fluxograma?
Implemente-o utilizando a linguagem C*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salAtual,taxaReajuste,salFinal;
    //Entrada de Dados
    printf("|Digite| Salario Atual:\nR$:");
    scanf("%f",&salAtual);
    printf("|Digite| Taxa de Reajuste:\n");
    scanf("%f",&taxaReajuste);
    //Processamento
    salFinal = salAtual*(1+taxaReajuste*0.01);
    //Saída de Dados;
    printf("|Salario sem reajuste| R$:%.2f\n",salAtual);
    printf("|Salario com reajuste| R$:%.2f\n",salFinal);
    return 0;
}