/*4) Um professor atribui pesos de 1 a 3 para as notas de 
três avaliações, respectivamente. Faça um algoritmo
que receba as notas e calcule e mostre a média ponderada. 
A média e as notas serão valores do tipo float.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float nota1;
    float nota2;
    float nota3;
    float media;
    //Entrada de Dados
    printf("Digite a nota da primeira prova:\n");
    scanf("%f",&nota1);
    printf("Digite a nota da segunda prova:\n");
    scanf("%f",&nota2);
    printf("Digite a nota da terceira prova:\n");
    scanf("%f",&nota3);
    //Processamento
    media = (nota1 + 2*nota2 + 3*nota3)/6;
    //Saída de Dados;
    printf("Sua media eh %.1f.",media);
    return 0;
}