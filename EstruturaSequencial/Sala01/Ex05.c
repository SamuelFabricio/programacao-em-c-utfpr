/*5) Tendo como entrada dois valores inteiros, elaborar um algoritmo 
para calcular e mostrar:
a) A soma desses valores;
b) A subtração do primeiro pelo segundo;
c) A multiplicação entre eles;
d) A divisão inteira do primeiro pelo segundo;
e) A divisão float do primeiro pelo segundo;
f) O resto da divisão do primeiro pelo segundo.*/

#include <stdio.h> 
#include <math.h>

int main()
{
    //Declarar Variáveis
    int numero1;
    int numero2;
    //Entrada de Dados
    printf("Digite um numero:\n");
    scanf("%i",&numero1);
    printf("Digite outro numero:\n");
    scanf("%i",&numero2);
    //Processamento
    //Saída de Dados;
    printf("%i + %i = %.1i.\n",numero1,numero2,numero1+numero2);
    printf("%.1i - %.1i = %.1i.\n",numero1,numero2,numero1-numero2);
    printf("%i x %i = %.1i.\n",numero1,numero2,numero1*numero2);
    printf("%.1i / %.1i + %.1i.\n",numero1,numero2,numero1/numero2);
    printf("Divisao Float: %.1i / %.1i = %.2f.\n",numero1,numero2,(float)numero1/(float)numero2);
    printf("Resto de %.1i / %.1i = %i.\n",numero1,numero2,numero1%numero2);
    return 0;
}