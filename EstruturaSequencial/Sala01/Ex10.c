/*10) Resolva as seguintes expressões matemáticas. X e Y são valores 
fornecidos pelo usuário. Calcule e mostre o resultado de cada expressão.
Reutilize variáveis, ou seja, terá apenas uma variável para armazenar
os resultados do tipo int e outra para armazenar o resultado do tipo 
float. Faz a primeira operação e já imprime o resultado e assim para
todas as demais operações. Atenção para os resultados que podem ser
valores float e para a prioridade dos operadores. Primeiro deve-se 
linearizar as expressões e depois implementar o algoritmo para 
calcular os resultados.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float x,y;
    //Entrada de Dados
    printf("|Digite| Valor de X|");
    scanf("%f",&x);
    printf("|Digite| Valor de Y|");
    scanf("%f",&y);
    //Processamento
    
    //Saída de Dados;
    //1ªExpressão
    printf("(X + Y / 2 ) * X'2 = %.1f\n",(((x+y)/2)*(x*x)));
    //2ªExpressão
    printf("(X + Y / X - Y) = %.1f\n",(x+y)/(x-y));
    //3ªExpressão
    printf("(X'2 + Y'3) / 2 = %.1f\n",(((x*x)+(y*y*y)))/2);
    //4ªExpressão
    printf("(X'3 / X'2) = %.1f\n",((x*x*x)/(x*x)));
    //5ªExpressão
    printf("Resto de X / Y = %.1i\n",((int)x%(int)y));
    printf("Resto de X / 3 = %.1i\n",(int)x%3);
    printf("Resto de Y / 5 = %.1i\n",(int)y%5);
    return 0;
}