/*) Calcular o valor do salário líquido de uma pessoa. Sobre o salário bruto incidem descontos de imposto de
renda e de INSS.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salBruto, salLiq, inss,impostoRenda;
    //Entrada de Dados
    printf("|Digite| Valor do salario bruto|\nR$:");
    scanf("%f",&salBruto);
    printf("|Digite| Taxa do INSS|\n");
    scanf("%f",&inss);
    printf("|Digite| Taxa de Imposto de renda|\n");
    scanf("%f",&impostoRenda);
    //Processamento
    salLiq = salBruto - salBruto*(inss*0.01)-salBruto*(impostoRenda*0.01);
    //Saída de Dados;
    printf("|Salario Liquido| R$%.2f |",salLiq);
    return 0;
}