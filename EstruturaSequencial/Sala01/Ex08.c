/*8) Faça um algoritmo que calcule e mostre a tabuada de 0 a 10 de um número inteiro 
digitado pelo usuário*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numerotab;
    //Entrada de Dados
    printf("|Digite| Quero ver a Tabuada do:");
    scanf("%i",&numerotab);
    //Processamento
    
    //Saída de Dados;
    printf("0 x %i = %i\n",numerotab,numerotab*0);
    printf("1 x %i = %i\n",numerotab,numerotab*1);
    printf("2 x %i = %i\n",numerotab,numerotab*2);
    printf("3 x %i = %i\n",numerotab,numerotab*3);
    printf("4 x %i = %i\n",numerotab,numerotab*4);
    printf("5 x %i = %i\n",numerotab,numerotab*5);
    printf("6 x %i = %i\n",numerotab,numerotab*6);
    printf("7 x %i = %i\n",numerotab,numerotab*7);
    printf("8 x %i = %i\n",numerotab,numerotab*8);
    printf("9 x %i = %i\n",numerotab,numerotab*9);
    printf("10 x %i = %i\n",numerotab,numerotab*10);
    return 0;
}