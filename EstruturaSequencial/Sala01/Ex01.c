/*1) Escreva um algoritmo que leia o comprimento (cm), a largura (cm) e a altura (cm) de uma caixa retangular
e calcule o seu volume (cm3
), cuja fórmula é:
Volume = Comprimento * Largura * Altura
Teste o seu algoritmo com as medidas abaixo:*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float altura;
    float largura;
    float comprimento;
    float volume;
    //Entrada de Dados
    printf("Digite a altura(cm):\n");
    scanf("%f",&altura);
    printf("Digite a largura(cm):\n");
    scanf("%f",&largura);
    printf("Digite o comprimento(cm):\n");
    scanf("%f",&comprimento);
    //Processamento
    volume = altura * largura * comprimento;
    //Saída de Dados;
    printf("O volume eh %.2f cm3.\n",volume);
    printf("Programa Finalizado!");
    return 0;
}
