/*7) Faça um algoritmo que receba uma temperatura em grau Celsius, 
e calcule e mostre essa temperatura em grau Fahrenheit. 
Sabe-se que F = (1.8 * C) + 32.
*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float celsius, fahrenheit;
    //Entrada de Dados
    printf("|Digite| Temperatura em 'C|");
    scanf("%f",&celsius);
    //Processamento
    fahrenheit = (1.8*celsius)+32;
    //Saída de Dados;
    printf("|%.1f'C| = |%.1f'F|",celsius,fahrenheit);
    return 0;
}