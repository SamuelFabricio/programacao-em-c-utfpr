/*2) Escreva um algoritmo que leia o raio de um círculo 
e calcule a sua circunferência (C = 2. π. r).*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float raio;
    float circunferencia;
    //Entrada de Dados
    printf("Digite o raio do circulo:\n");
    scanf("%f",&raio);
    //Processamento
    circunferencia = 2 * 3.14 * raio;
    //Saída de Dados;
    printf("A circunferencia eh %.2f.\n",circunferencia);
    return 0;
}