/*3) Escreva um algoritmo que leia o valor de uma prestação e 
da taxa de juros cobrada pelo atraso daprestação. Calcule o valor 
a ser pago por meio da fórmula:
Valor com juros = Prestação + (Prestação * Taxa /100)*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float prestacao;
    float taxa;
    float prestacaoatualizada;
    //Entrada de Dados
    printf("Digite o valor da prestacao:\n");
    scanf("%f",&prestacao);
    printf("Digite a taxa:\n");
    scanf("%f",&taxa);
    //Processamento
    prestacaoatualizada = prestacao * (taxa*0.01+1);
    //Saída de Dados;
    printf("Sua Prestacao atualizada eh R$:%.2f.\n",prestacaoatualizada);
    return 0;
}