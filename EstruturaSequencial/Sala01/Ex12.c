/*12) Calcule e mostre a média de idade de três pessoas.*/

#include <stdio.h>
#include <math.h>

int main()
{
    //Declarar Variáveis
    float idadeMedia;
    int idade1,idade2,idade3;
    //Entrada de Dados
    printf("|Digite| 1'Idade:");
    scanf("%i",&idade1);
    printf("|Digite| 2'Idade:");
    scanf("%i",&idade2);
    printf("|Digite| 3'Idade:");
    scanf("%i",&idade3);
    //Processamento
    idadeMedia = round(idade1 + idade2 + idade3)/3;
    //Saída de Dados;
    printf("A Idade media eh %.0f anos.",idadeMedia);
    return 0;
}