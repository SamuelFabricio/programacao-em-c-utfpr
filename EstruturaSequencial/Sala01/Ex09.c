/*9) Leia dois valores A e B e efetue a troca do conteúdo das 
duas variáveis lidas, de forma que a variável A passe a ter o valor 
de B e a variável B o valor de A. Mostre o conteúdo das variáveis 
antes e depois da troca.
*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int A, B;
    //Entrada de Dados
    printf("|Digite A|:");
    scanf("%i",&A);
    printf("|Digite B|:");
    scanf("%i",&B);
    //Processamento
    
    //Saída de Dados;
    printf("ANTES|A|= %i AGORA|A|= %i\n",A,A-A+B);
    printf("ANTES|B|= %i AGORA|B|= %i\n",B,B-B+A);
    return 0;
}