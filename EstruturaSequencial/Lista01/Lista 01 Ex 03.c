#include <stdio.h>

int main()
{
    float salariofixo;
    float comissao;
    float carrosvendidos;
    float totaldevendas;
    float salario;
    printf("Informe o valor do salario fixo.\n");
    scanf("%f",&salariofixo);
    printf("Informe o valor da comissao por carro vendido.\n");
    scanf("%f",&comissao);
    printf("Informe o numero de carros vendidos.\n");
    scanf("%f",&carrosvendidos);
    printf("Informe o valor total de vendas.\n");
    scanf("%f",&totaldevendas);
    salario = (salariofixo + comissao*carrosvendidos + totaldevendas*0.05);
    printf("O salario liquido eh R$%.2f.\n",salario);
    printf("Programa Finalizado.\n");
    
    return 0;
}
