#include <stdio.h>

int main()
{
    float raio;
    float altura;
    float volume;
    printf("Digite o raio do Cilindro:\n");
    scanf("%f",&raio);
    printf("Digite a altura do Cilindro:\n");
    scanf("%f",&altura);
    volume = (3.1415*(raio*raio))*altura;
    printf("O volume do Cilindro eh %f.\n",volume);
    return 0;
}
