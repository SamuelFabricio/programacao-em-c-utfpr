#include <stdio.h>

int main()
{
    float valorinicial;
    float percentual;
    float valorfinal;
    printf("Digite o valor do produto e o percentual a ser usado:\n");
    scanf("%f""%f", &valorinicial, &percentual);
    valorfinal = (valorinicial)*(1+(percentual/100));
    printf("O valor inicial do produto eh R$%.2f.\n",valorinicial);
    printf("O percentual usado foi de %.0f%%.\n",percentual);
    printf("O valor final do produto eh R$%.2f.\n",valorfinal);
    return 0;
}
