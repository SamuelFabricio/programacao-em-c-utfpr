/*8) Faça um programa que leia o salário bruto mensal de um funcionário,
calcule e mostre os valores conforme o exemplo a seguir. Observação: 
É possível fazer esse programa utilizando somente três variáveis: 
uma para ler o salário bruto, outra para os descontos e outra para o 
salário líquido.
*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float salBruto, salLiquido, descontos;
    //Entrada de Dados
    printf("|Digite| Salario Bruto |");
    scanf("%f",&salBruto);
    //Processamento
    salLiquido = salBruto*(1-0.29);
    //Saida de Dados
    printf("Salario Bruto        R$:%.2f\n",salBruto);
    printf("(-) IR        (15%%): R$:%.2f\n",salBruto*0.15);
    printf("(-) INSS      (11%%): R$:%.2f\n",salBruto*0.11);
    printf("(-) SINDICATO (3%%) : R$:%.2f\n",salBruto*0.03);
    printf("Salario Liquido      R$:%.2f\n",salLiquido);
    return 0;
}