/*9) Escreva um programa que o leia o número de horas trabalhadas por 
um funcionário, o valor por hora, o número de filhos com idade menor 
do que 14 anos, o valor do salário família por filho e calcule e 
mostre o salário desse funcionário*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float horasTrab, valorHora, filhosMenores, salFamilia;
    //Entrada de Dados
    printf("|Digite| Horas Trabalhadas |");
    scanf("%f",&horasTrab);
    printf("|Digite| Valor da Hora Trabalhada |");
    scanf("%f",&valorHora);
    printf("|Digite| Quantidade de Filhos Menores de 14 anos |");
    scanf("%f",&filhosMenores);
    printf("|Digite| Valor do Salario Familia |");
    scanf("%f",&salFamilia);
    //Processamento
    
    //Saída de Dados;
    printf("|Salario Liquido| R$%.2f |",(horasTrab*valorHora)+(filhosMenores*salFamilia));
    return 0;
}