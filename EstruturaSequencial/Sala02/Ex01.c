/*1) Ler dois números float e apresentar, sem utilizar funções 
matemáticas prontas:
a) A divisão do primeiro número pelo segundo, armazenando somente
 a parte inteira do número.
b) A soma dos dois números com o resultado arredondado para 
o próximo número inteiro.
Obs.: As variáveis de resultados devem ser do tipo int.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float numero1,numero2;
    int parteInt, arredondamento;
    //Entrada de Dados
    printf("|Digite| Primeiro numero Float |");
    scanf("%f",&numero1);
    printf("|Digite| Segundo numero Float |");;
    scanf("%f",&numero2);
    //Processamento
    parteInt = numero1/numero2;
    arredondamento = numero1/numero2;
    //Saída de Dados;
    printf("Divisao inteira: %i.\n",parteInt);
    printf("Arrendondamento: %i.\n",arredondamento);
    return 0;
}