/*4) Escreva um programa que, dados a quantidade de litros de 
combustível utilizada, os quilômetros percorridos por um automóvel 
e o valor do litro de combustível, calcule quantos quilômetros o 
veículo percorreu por litro de combustível e o valor gasto em reais 
por km.*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float km, consumo, valorCombus;
    //Entrada de Dados
    printf("|Digite| Distancia percorrida(Km)|\n");
    scanf("%f",&km);
    printf("|Digite| Consumo de Combustivel(L)|\n");
    scanf("%f",&consumo);
    printf("|Digite| Valor do Combustivel(R$)|\n");
    scanf("%f",&valorCombus);
    //Processamento
    
    //Saída de Dados;
    printf("|O automovel fez %.1f km/L|\n",km/consumo);
    printf("|Gastou R$%.2f/Km|\n",(valorCombus*consumo)/km);
    printf("|Gastou R$%.2f de combustivel na viagem|",valorCombus*consumo);
    return 0;
}