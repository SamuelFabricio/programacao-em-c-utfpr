/*7) Um viajante de carro fará o trajeto entre duas cidades e ao 
término da viagem deseja saber:
a) Quantas vezes foi necessário abastecer o carro. 
(Use a função ceil() da biblioteca math.h para arredondaro valor 
para cima)
b) Quantos litros foram consumidos para percorrer a distância indicada.
c) Quantos litros restaram no tanque após a chegada ao destino.
Faça um programa que leia a distância entre as duas cidades, a
 capacidade do tanque e o consumo médio
do veículo, calcule e mostre as informações solicitadas.
*/
#include <stdio.h>
#include <math.h>

int main()
{
    //Declarar Variáveis
    float distancia,capacidade,consumo;
    //Entrada de Dados
    printf("|Digite| Distancia Percorrida(Km) |");
    scanf("%f",&distancia);
    printf("|Digite| Capacidade de combustivel do automovel(L) |");
    scanf("%f",&capacidade);
    printf("|Digite| Consumo do automovel(Km/L) |");
    scanf("%f",&consumo);
    //Processamento
    
    //Saída de Dados;
    printf("|%.1f Litros de combustivel para percorrer %.1f(Km)|\n",distancia/consumo,distancia);
    printf("|Abasteceu %.0f vezes|\n", ceil(distancia/(capacidade*consumo)));
    printf("|Restou %.1f Litros no tanque|\n",(capacidade * ceil(distancia/(capacidade*consumo)) - (distancia/consumo) ));
    return 0;
}