/*5) Escreva um programa para ler o número de votos brancos, nulos 
(incluem eleitores ausentes) e válidos de uma eleição. 
Calcular e mostrar o percentual que cada um (brancos, nulos e válidos) 
representa em relação ao total de eleitores. 
Lembrar que os valores dos percentuais podem não ser inteiros.
*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float votosValidos, votosBrancos,VotosNulos, numeroMagico,processamento;
    //Entrada de Dados
    printf("|Digite| Votos Validos |");
    scanf("%f",&votosValidos);
    printf("|Digite| Votos Brancos |");
    scanf("%f",&votosBrancos);
    printf("|Digite| Votos Nulos |");
    scanf("%f",&VotosNulos);
    //Processamento
    processamento = (votosValidos + votosBrancos + VotosNulos)/100;
    //Saída de Dados;
    printf("|Porcentagem de votos validos| %.2f%%\n",votosValidos/processamento);
    printf("|Porcentagem de votos brancos| %.2f%%\n",votosBrancos/processamento);
    printf("|Porcentagem de votos Nulos  | %.2f%%\n",VotosNulos/processamento);
    return 0;
}