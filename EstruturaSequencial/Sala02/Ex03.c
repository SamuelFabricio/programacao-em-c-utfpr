/*3) Escreva um programa que calcule o valor da conversão 
para dólares de um valor lido em reais*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float valorDollar, valorReais;
    //Entrada de Dados
    printf("|Digite| Valor Dollar |");
    scanf("%f",&valorDollar);
    printf("|Digite| Valor Reais |");
    scanf("%f",&valorReais);
    //Processamento
    
    //Saída de Dados;
    printf("|Conversao de R$%.2f para US$%.2f|",valorReais,valorReais/valorDollar);
    return 0;
}