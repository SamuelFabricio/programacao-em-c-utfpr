/*6) Faça um programa que leia o preço de uma mercadoria com diferença
de um mês (ler o valor da mercadoria no mês passado e no mês atual) 
e calcule a taxa de inflação mensal dessa mercadoria. A inflação
é o percentual da diferença de preços (atual menos o anterior) 
sobre o preço anterior*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float precoAntigo, novoPreco;
    //Entrada de Dados
    printf("|Digite| Preco Antigo | \nR$:");
    scanf("%f",&precoAntigo);
    printf("|Digite| Preco Novo | \nR$:");
    scanf("%f",&novoPreco);
    //Processamento
    
    //Saída de Dados;
    printf("|Inflacao do produto| %.1f%%\n",(precoAntigo/100)*(novoPreco-precoAntigo));
    return 0;
}