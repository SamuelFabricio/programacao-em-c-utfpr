/*2) O custo ao consumidor de um carro novo é a soma do custo de
 fábrica com a percentagem do distribuidor e a percentagem dos 
impostos (ambas aplicadas sobre o custo de fábrica). 
Escrever um programa para, a partir do custo de fábrica do carro, 
calcular e mostrar o custo ao consumidor*/

#include <stdio.h>

int main()
{
    //Declarar Variáveis
    float valorFabrica, taxaDistribuidor, taxaImpostos, valorConsumidor;
    //Entrada de Dados
    printf("|Digite| Valor de Fabrica |");
    scanf("%f",&valorFabrica);
    printf("|Digite| Taxa do Distribuidor |");
    scanf("%f",&taxaDistribuidor);
    printf("|Digite| Taxa do Imposto |");
    scanf("%f",&taxaImpostos);
    //Processamento
    valorConsumidor = valorFabrica*(1+(taxaDistribuidor*0.01)+(taxaImpostos*0.01));
    //Saída de Dados;
    printf("|VALOR PARA CONSUMIDOR| R$ %.2f |",valorConsumidor);
    return 0;
}