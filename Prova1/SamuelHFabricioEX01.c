/*Questão 1 (3,4 pontos) – Faça um programa que apresente um menu de opções e 
implemente as seguintes funcionalidades, detalhadas a seguir:
a) Apresentar divisores do número.
b) Verificar se o número é perfeito.
c) Soma dos divisores.
d) Verificar se os números são múltiplos.
*/
#include <stdio.h>

int main()
{
    //Declarar Variáveis
    int numero=1, opcoes, i,soma=0;
    //Entrada de Dados
    printf("\tMENU\n[1]Numero de divisores\n[2]Verificar se o numero eh perfeito\n[3]Soma dos divisores\n[4]Verificar se os numeros sao multiplos\n");
    scanf("%d",&opcoes);
    switch (opcoes)
    {
    case 1:
        do
        {
            printf("Informe um numero (0 para fechar o programa)\n");
            scanf("%d",&numero);
            for (i = 1; i <= numero; i++)
            {
                if (numero%i==0)
                {
                    soma++;
                }    
            }
            printf("O numero %d possui %d divisor(es)\n",numero,soma);
            soma=0;
        } while (numero!=0);
        
        break;
    case 2:
        while (numero!=0)
        {
            printf("Informe um numero (0 para fechar o programa)\n");
            scanf("%d",&numero);
            for (i = 1; i < numero; i++)
            {
                if (numero%i==0)
                {
                    soma=soma+i;
                }
            }
            if (soma==numero)
            {
                printf("O numero %d eh perfeito\n",numero);
            }
            else
            {
                printf("O numero %d nao eh perfeito\n",numero);
            }
        }
        break;
    case 3:
        /* code */
        break;
    case 4:
        /* code */
        break;
    
    default:
        break;
    }
    
    //Processamento
    
    //Saída de Dados;
    printf("PROGRAMA FINALIZADO");
    return 0;
}