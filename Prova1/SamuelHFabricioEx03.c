/*Questão 3 (3,3 pontos) - Ler dois números positivos que representam os 
limites inferior e superior de um intervalo. Nesse intervalo, apresentar os 
valores que são quadrados perfeitos. Um número é quadrado perfeito se
possui como raiz quadrada um valor inteiro. Fazer a média das raízes dos 
quadrados perfeitos do intervalo. A função para obter a raiz quadrada 
é sqrt() e está em na biblioteca math.h*/

#include <stdio.h>
#include <math.h>

int main()
{
    //Declarar Variáveis
    int limiteInferior, limiteSuperior, i, raiz, soma=0, cont=0, media;
    //Entrada de Dados
    printf("|Informe o valor inicial do intervalo|");
    scanf("%d",&limiteInferior);
    printf("|Informe o valor final do intervalo|");
    scanf("%d",&limiteSuperior);
    //Processamento
    for ( i = limiteInferior; i <= limiteSuperior; i++)
    {
        printf("%d \t(raiz %d)\n",i*i,i);
        raiz = sqrt(i);
        soma = soma+i;
        cont++;
    }
    media = soma/cont;
    //Saída de Dados;
    printf("A media das raizes dos quadrados perfeitos: %d",media);
    return 0;
}